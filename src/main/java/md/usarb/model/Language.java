package md.usarb.model;

public enum Language {
    RO, RU, EN;
}
