package md.usarb.rest;

import md.usarb.model.Book;
import md.usarb.repository.BookRepository;

import javax.inject.Inject;
import javax.inject.Named;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;

@Path("/books")
@Produces({"application/json"})
public class BookController {

    @Inject
    @Named("bookRepository")
    BookRepository bookRepository;

    @Path("/")
    @POST
//    @Produces({"application/json"})
    public Response create() throws URISyntaxException {
        return Response.created(new URI("/books/1")).build();
    }

    @Path("/")
    @GET
//    @Produces({"application/json"})
    public Response read() {
        bookRepository.create(null);
        return Response.ok().entity(bookRepository.findAll()).build();
    }

    @Path("/{bookId}")
    @GET
//    @Produces({"application/json"})
    public Response readById(@PathParam("bookId") long bookId) {
        Book book = bookRepository.findById(bookId);
        if (book != null) {
            return Response.ok().entity(book).build();
        } else {
            return Response.noContent().build();
        }
    }

    @Path("/{bookId}")
    @PUT
//    @Produces({"application/json"})
    public Response update(@PathParam("bookId") long bookId) {
        return Response.noContent().build();
    }

    @Path("/{bookId}")
    @DELETE
//    @Produces({"application/json"})
    public Response delete(@PathParam("bookId") long bookId) {
        return Response.noContent().build();
    }
}
