package md.usarb.repository;

import md.usarb.model.Book;
import md.usarb.model.Language;

import javax.ejb.Stateless;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceContextType;
import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collection;

@Named("bookRepository")
@Transactional
//@Stateless
public class BookRepository {

//    @Inject
@PersistenceContext(unitName = "bookStore")
    private EntityManager entityManager;

    public Collection<Book> findAll() {
        Collection<Book> books = new ArrayList<>();

        Book book = new Book();
        book.setTitle("Fundaţia I. Fundația");
        book.setDescription("Hari Seldon, întemeietorul psihoistoriei, prezice prăbuşirea Imperiului Galactic şi instaurarea unei ere de barbarie care va dura treizeci de milenii. Cu ajutorul colaboratorilor săi, Seldon înfiinţează Fundaţia Enciclopediei Galactice, a cărei misiune este să salveze cunoştinţele a 12 000 de ani de evoluţie. Fundaţia este exilată pe Terminus, o planetă de la marginea Galaxiei, de unde se implică mersul istoriei, transformând ştiinţa în religie, iar savanţii în preoţi care s-o răspândească în lumile Imperiului");
        book.setPrice(BigDecimal.valueOf(25.50));
        book.setIsbn("9786069351031");
        book.setPublicationDate(LocalDate.parse("2017-09-01"));
        book.setNumberOfPages(264);
        book.setImageUrl("https://www.editura-paladin.ro/image/2277/fundatia-cover_big.jpg");
        book.setLanguage(Language.RO);
        books.add(book);

        book = new Book();
        book.setTitle("The Hobbit");
        book.setDescription("Bilbo Baggins is a hobbit who enjoys a comfortable, unambitious life, rarely traveling any farther than his pantry or cellar. But his contentment is disturbed when the wizard Gandalf and a company of dwarves arrive on his doorstep one day to whisk him away on an adventure. They have launched a plot to raid the treasure hoard guarded by Smaug the Magnificent, a large and very dangerous dragon. Bilbo reluctantly joins their quest, unaware that on his journey to the Lonely Mountain he will encounter both a magic ring and a frightening creature known as Gollum.");
        book.setPrice(BigDecimal.valueOf(11.16));
        book.setIsbn("054792822X");
        book.setPublicationDate(LocalDate.parse("2012-09-18"));
        book.setNumberOfPages(300);
        book.setImageUrl("https://images-na.ssl-images-amazon.com/images/I/51uLvJlKpNL._SX321_BO1,204,203,200_.jpg");
        book.setLanguage(Language.EN);
        books.add(book);

        book = new Book();
        book.setTitle("The Great Gatsby");
        book.setDescription("A true classic of twentieth-century literature, this edition has been updated by Fitzgerald scholar James L.W. West III to include the author’s final revisions and features a note on the composition and text, a personal foreword by Fitzgerald’s granddaughter, Eleanor Lanahan—and a new introduction by two-time National Book Award winner Jesmyn Ward.");
        book.setPrice(BigDecimal.valueOf(11.84));
        book.setIsbn("9780743273565");
        book.setPublicationDate(LocalDate.parse("2004-09-30"));
        book.setNumberOfPages(180);
        book.setImageUrl("https://images-na.ssl-images-amazon.com/images/I/51vv75oglyL._SX326_BO1,204,203,200_.jpg");
        book.setLanguage(Language.EN);
        books.add(book);

        return books;
    }

    public Book findById(long bookId) {

        return null;
    }

    public Long create(Book book) {
        Book book1 = new Book();
        book1.setTitle("123");
        entityManager.persist(book1);
        return null;
    }

    public void update(Book book) {

    }

    public void deleteById(long bookId) {

    }
}
